# CHANGELOG

## Version : 2.1.0

- fix: gitlab-ci : See merge request getto-systems-labo/container/dockerfile!50


## Version : 2.0.1



## Version : 2.0.0

upgrade: os! See merge request getto-systems-labo/container/dockerfile!45
remove: changelog See merge request getto-systems-labo/container/dockerfile!44


## Version : 1.6.0



## Version : 1.5.0



## Version : 1.4.0



## Version : 1.3.0



## Version : 1.2.0

fix: gitlab-ci See merge request getto-systems-labo/container/dockerfile!43

# Version : 1.1.0

fix: neovim install

# Version : 1.0.0

fix: base on ubuntu

# Version : 0.7.8

fix: push_latest

# Version : 0.7.7

fix: push_latest

# Version : 0.7.6

fix: push_latest

# Version : 0.7.5

fix: push_latest

# Version : 0.7.4

fix: push_latest

# Version : 0.7.3

fix: push_latest

# Version : 0.7.2

fix: push_latest

# Version : 0.7.1

fix: push_latest

# Version : 0.7.0

add: push_latest job

# Version : 0.6.0

fix: gitlab-ci

# Version : 0.5.0

fix: gitlab-ci

# Version : 0.4.0

add: dockle and trivy test

# Version : 0.3.1

fix: vulnerabilities

# Version : 0.3.0

fix: env vars

# Version : 0.2.0

fix: use setpriv

# Version : 0.1.0

add: Dockerfile

